; Example make file for the module.
api = 2
core = 7.x
projects[bean][version] = 1.2
projects[bean][subdir] = contrib

projects[image_link_formatter][version] = 1.0
projects[image_link_formatter][subdir] = contrib

projects[link][version] = 1.1
projects[link][subdir] = contrib

projects[linkit][version] = 2.6
projects[linkit][subdir] = contrib

projects[linkit_picker][version] = 1.0
projects[linkit_picker][subdir] = contrib

projects[linkit_target][version] = 1.0
projects[linkit_target][subdir] = contrib
