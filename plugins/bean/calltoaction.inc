<?php
/**
 * @file
 *   A bean calltoaction plugin.
 */

class Beanc2aBean extends BeanPlugin {
  /**
   * Implements BeanPlugin::values().
   */
  public function values() {
    return array(
      'include_default_css' => TRUE,
      'caption_click' => array(
        'enable' => FALSE,
        'target' => '.caption',
      ),
    );
  }

  /**
   * Implements BeanPlugin::form().
   */
  public function form($bean, $form, &$form_state) {
    $form = array();


    $form['include_default_css'] = array(
      '#type' => 'checkbox',
      '#title' => t('Include default CSS'),
      '#description' => t('Add the default CSS styles that come with Beanc2a.'),
      '#default_value' => $bean->include_default_css,
    );
    return $form;
  }

  /**
   * Implements BeanPlugin::view().
   */
  public function view($bean, $content, $view_mode = 'full', $langcode = NULL) {
    if ($bean->include_default_css) {

      drupal_add_css(drupal_get_path('module', 'beanc2a') . '/css/beanc2a.css', array('group' => CSS_DEFAULT, 'every_page' => TRUE));
    }
    return $content;
  }
}
